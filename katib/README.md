## Example - Katib

Running hyperparameter tunning with a distributed Pytorch job.

### Setup

- Open a server on https://ml.cern.ch/_/jupyter
    - Select one of JupyterLab images
- Open a terminal in the notebook server and clone this repo
    - `git clone https://gitlab.cern.ch/ai-ml/examples.git; cd katib`
- Authenticate with kerberos
    - `kinit <cernid>`
- When kerberos has been refreshed, remove any old secret before creating a new one
    - `kubectl delete secret krb-secret`
- Create a kerberos secret for Kubernetes
    - `kubectl create secret generic krb-secret --from-file=/tmp/krb5cc_1000`

### How to run?

#### UI
- Navigate to https://ml.cern.ch/_/katib/
- Click `NEW EXPERIMENT`
- At the bottom of the page click `Edit and submit YAML`
- Paste the contents of `hp-tuning.yaml` job file in the box and click `CREATE` 
- The progress can be seen in the UI on https://ml.cern.ch/_/katib/experiment

#### CLI
- `kubectl apply -f hp-tuning.yaml`
- Monitor with:
    - `kubectl get experiment`
    - `kubectl get trials`
    - `kubectl get pytorchjob`
    - `kubectl get pods`

### Documentation
- https://ml.docs.cern.ch/katib
- https://www.kubeflow.org/docs/components/katib
