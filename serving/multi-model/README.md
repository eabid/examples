# Multi model serving
Kubeflow allows you to serve multiple models from a single inference server. You will be able to make use of the same amount of resources to serve multiple models. 

Example from here: https://github.com/kserve/kserve/tree/release-0.6/docs/samples/multimodelserving/triton.  

# Create inference service
The metadata in `inferenceservice.yaml` needs to be edited with a respective which will be used later to access your model. After that 

```
kubectl apply -f inferenceservice.yaml
```

# Serving Models with the newly created inference service
You will be able to serve multiple models by replicating `cifar10.yaml` and just changing the details for your model location.

```
kubectl apply -f cifar10.yaml
```

Follow the same steps for `simple-string.yaml`:

```
kubectl apply -f simple-string.yaml
```

You have now deployed 2 models on the same inference service.


# Check Status
To check for the status of the models run 
```
kubectl get trainedmodels.serving.kubeflow.org
```

You should now be able to see the 2 newly deployed models with their URLS.
**REMEMBER** The 'ready' status for both these models should be true inorder for you to access them.

# Access the models and inference service

## For accessing the `cifar10` model:

```
curl http://triton-mms.dgolubov.svc.cluster.local/v2/models/cifar10
```

Expected output:
```
{"name":"cifar10","versions":["1"],"platform":"pytorch_libtorch","inputs":[{"name":"INPUT__0","datatype":"FP32","shape":[-1,3,32,32]}],"outputs":[{"name":"OUTPUT__0","datatype":"FP32","shape":[-1,10]}]}
```

## For accessing the `simple-string` model:

```
curl http://triton-mms.dgolubov.svc.cluster.local/v2/models/simple-string
```

Expected output:
```
{"name":"simple-string","versions":["1"],"platform":"tensorflow_graphdef","inputs":[{"name":"INPUT0","datatype":"BYTES","shape":[-1,16]},{"name":"INPUT1","datatype":"BYTES","shape":[-1,16]}],"outputs":[{"name":"OUTPUT0","datatype":"BYTES","shape":[-1,16]},{"name":"OUTPUT1","datatype":"BYTES","shape":[-1,16]}]}
```
