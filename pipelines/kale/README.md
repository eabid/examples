## Example - mnist-kale

### What is it about?

Create and run a pipeline by annotating cells and using KALE Jupyter Lab extension. <br/>

### How to run?

- Open a running NB server
- Open Terminal
- Login to kerberos with kinit:
```
kinit CERN-USER-ID
```
- Remove old kerberos secret before creating a new one
```
kubectl delete secret krb-secret
```
- Create a kerberos secret for Kubernetes
```
kubectl create secret generic krb-secret --from-file=/tmp/krb5cc_1000
```
- Open **mnist-kale/mnist-kale-katib.ipynb** in your Notebook server
- On the left side, select Kubeflow Pipelines Deployment Panel
- Toggle Enable
- Select Experiment (existing or new)
- Write Pipeline name and Pipeline description
- Untoggle *HP Tuning with Katib*
- Click Compile and Run at the bottom of the page
- After successfull compilation, click View
- Inspect and debug your pipeline via Pipeline log
