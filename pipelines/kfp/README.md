## Example - mnist-kfp

### What is it about?

Create an ML pipeline using Python code, from Jupyter notebook. <br/>
Demonstrate running two models in parallel with a pipeline. <br/>

Workflow: read data, preprocess data, train two models in parallel, evaluate models results.

### How to run?

- Open **mnist-kfp/mnist-kfp.ipynb** in your Notebook server
- Run all the cells
- Monitor run by clicking the link produced by the last cell
