import kfp
import os

client = kfp.Client(host='https://ml.cern.ch/pipeline',
                    cookies='authservice_session=...')
print('client obtained:' + str(client))

pipeline_name = 'hello-remote-pipeline'
experiment_name = pipeline_name
namespace = 'dgolubov'

try:
    experiment_id = client.get_experiment(experiment_name=experiment_name, 
                                          namespace=namespace).id
    print('experiment id obtained: ' + str(experiment_id))
except:
    client.create_experiment(experiment_name, namespace=namespace)
    print('experiment created')

    experiment_id = client.get_experiment(experiment_name=experiment_name, 
                                          namespace=namespace).id
    print('experiment id obtained: ' + str(experiment_id))
    
pipeline_id = client.get_pipeline_id(name=pipeline_name)

if pipeline_id is not None:
    print('pipeline id obtained: ' + str(pipeline_id))
else:
    client.upload_pipeline(pipeline_package_path='dag_diamond.yaml', 
                           pipeline_name=pipeline_name)
    print('pipeline uploaded')

    pipeline_id = client.get_pipeline_id(name=pipeline_name)
    print('pipeline id obtained: ' + str(pipeline_id))

client.run_pipeline(experiment_id=str(experiment_id),
                    job_name='hello-remote-run',
                    pipeline_id=str(pipeline_id))
print('run submitted')
